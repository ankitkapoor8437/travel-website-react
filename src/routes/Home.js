import React from 'react';
import Navbar from '../components/Navbar';
import Hero from '../components/hero-section/Hero';

function Home() {
    return (
        <div>
            <Navbar />
             {/* Hero Section*/}
             <Hero/>
        </div>
    )
}

export default Home;